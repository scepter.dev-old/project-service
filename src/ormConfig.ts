import { ConnectionOptions } from "typeorm";
import * as path from "path";
import * as dotEnv from 'dotenv'

dotEnv.config();

const config: ConnectionOptions = {
    type: 'mysql',
    host: process.env.TYPEORM_HOST,
    port: +process.env.TYPEORM_PORT,
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    synchronize: false,
    migrationsRun: false,
    entities: [path.resolve(__dirname, 'entity/**.*')],
    migrations: [path.resolve(__dirname, 'migration/**')],
    cli: {
        entitiesDir: "src/entity",
        migrationsDir: "src/migration"
    }
}

export = config;
