import { DevmodeMiddleware } from './devmode.middleware';

describe('DevmodeMiddleware', () => {
  it('should be defined', () => {
    expect(new DevmodeMiddleware()).toBeDefined();
  });
});
