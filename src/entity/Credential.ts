import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Repository} from "./Repository";

@Entity()
export class Credential extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('varchar', {length: 255, nullable: false})
    username: string;

    @Column('varchar', {length: 255, nullable: false, select: false})
    password: string;

    @ManyToOne(type => Repository, repository => repository.credentials, {onDelete: 'CASCADE'})
    repository: Repository;
}
