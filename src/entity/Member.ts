import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { Project } from "./Project";

@Entity()
export class Member extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ManyToOne(type => Project, project => project.members, {onDelete: 'CASCADE'})
    project: Project;

    @Column('varchar')
    user: string;

}
