import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Repository} from "./Repository";
import { Member } from "./Member";

@Entity()
export class Project extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('varchar', {length: 255, nullable: false})
    displayName: string;

    @Column('varchar', {length: 255, nullable: false})
    name: string;

    @Column('text')
    description: string;

    @Column('varchar', {length: 255, nullable: false})
    owner: string;

    @OneToMany(type => Repository, repository => repository.project, {cascade: true})
    repositories: Repository[];

    @OneToMany(type => Member, member => member.project, {cascade: true})
    members: Member[]
}
