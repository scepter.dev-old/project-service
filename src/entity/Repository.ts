import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Project} from "./Project";
import {Credential} from "./Credential";

@Entity()
export class Repository extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('varchar', {nullable: false})
    displayName: string;

    @Column('varchar', {nullable: false})
    name: string;

    @Column('varchar', {nullable: false, default: 'master'})
    defaultBranch: string;

    @ManyToOne(type => Project, project => project.repositories, {onDelete: 'CASCADE'})
    project: Project;

    @OneToMany(type => Credential, credential => credential.repository, {cascade: true})
    credentials: Credential[]
}
