import {
    BadRequestException, ConflictException,
    ForbiddenException,
    Injectable,
    NotFoundException,
    UnauthorizedException
} from '@nestjs/common';
import {Project} from "../entity/Project";
import {Repository} from "../entity/Repository";
import * as bcrypt from "bcrypt";
import {Credential} from "../entity/Credential";

@Injectable()
export class RepositoryService {
    async create(name: string, displayName: string, projectId: string, user: string): Promise<Repository> {
        const foundProject = await Project.findOne({where: {id: projectId}});

        if(!foundProject) {
            throw new NotFoundException();
        }

        if(user !== foundProject.owner) {
            throw new ForbiddenException();
        }

        if(name === "" || displayName === "") {
            throw new BadRequestException("empty_field")
        }

        const foundRepo = await Repository.findOne({where: {project: projectId, name}});

        if(foundRepo) {
            throw new ConflictException('repo_already_exists')
        }

        const repository = new Repository();
        repository.name = name;
        repository.displayName = displayName;
        repository.project = foundProject;
        repository.credentials = [];
        await repository.save();
        return repository;
    }

    async remove(projectId: string, repoId: string, user: string): Promise<Repository> {
        const foundProject = await Project.findOne({where: {id: projectId}, relations: ['repositories']});

        if(!foundProject) {
            throw new NotFoundException("project_not_found");
        }

        if(user !== foundProject.owner) {
            throw new ForbiddenException();
        }

        const foundRepo = foundProject.repositories.find(repo => repo.id === repoId);

        if(!foundRepo) {
            throw new NotFoundException("repo_not_found");
        }

        await foundRepo.remove();
        return foundRepo;
    }

    async listCredentials(project: string, repository: string, user: string): Promise<Credential[]> {
        const foundProject = await Project.findOne({where: {id: project}, relations: ['repositories', 'repositories.credentials']});

        if(!foundProject) {
            throw new NotFoundException("project_not_found");
        }

        if(user !== foundProject.owner) {
            throw new ForbiddenException();
        }

        const foundRepo = foundProject.repositories.find(repo => repo.id === repository);

        if(!foundRepo) {
            throw new NotFoundException("repo_not_found");
        }

        return foundRepo.credentials;
    }

    async createCredential(project: string, repository: string, username: string, password: string, user: string): Promise<Credential> {
        const foundProject = await Project.findOne({where: {id: project}, relations: ['repositories', 'repositories.credentials']});

        if(!foundProject) {
            throw new NotFoundException("project_not_found");
        }

        if(user !== foundProject.owner) {
            throw new ForbiddenException();
        }

        const foundRepo = foundProject.repositories.find(repo => repo.id === repository);

        if(!foundRepo) {
            throw new NotFoundException("repo_not_found");
        }

        if(username === "" || password === "") {
            throw new BadRequestException("empty_field")
        }

        const foundCredential = await foundRepo.credentials.find(cred => cred.username === username);

        if (foundCredential) {
            throw new BadRequestException("credential_exists")
        }

        const credential = new Credential();
        credential.username = username;
        credential.password = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
        credential.repository = foundRepo;
        return credential.save();
    }

    async deleteCredential(project: string, repository: string, credential: string, user: string): Promise<Credential> {
        const foundProject = await Project.findOne({where: {id: project}, relations: ['repositories', 'repositories.credentials']});

        if(!foundProject) {
            throw new NotFoundException("project_not_found");
        }

        if(user !== foundProject.owner) {
            throw new ForbiddenException();
        }

        const foundRepo = foundProject.repositories.find(repo => repo.id === repository);

        if(!foundRepo) {
            throw new NotFoundException("repo_not_found");
        }

        const foundCredential = await foundRepo.credentials.find(cred => cred.id === credential);

        if (!foundCredential) {
            throw new NotFoundException("credential_not_found")
        }

        await foundCredential.remove();
        return foundCredential;
    }

    async verifyCredentials(project: string, repository: string, username: string, password: string): Promise<Boolean> {
        const foundProject = await Project.findOne({where: {name: project}, relations: ['repositories', 'repositories.credentials']});

        if(!foundProject) {
            throw new NotFoundException("project_not_found");
        }

        const foundRepo = foundProject.repositories.find(repo => repo.name === repository);

        if(!foundRepo) {
            throw new NotFoundException("repo_not_found");
        }

        const repoCredential = foundRepo.credentials.find(cred => cred.username === username);
        if(!repoCredential) {
            throw new UnauthorizedException();
        }

        const foundCredential = await Credential.findOne({where: {id: repoCredential.id}, select: ['password']});

        if(!bcrypt.compareSync(password, foundCredential.password)){
            throw new UnauthorizedException();
        }

        return true;
    }


}
