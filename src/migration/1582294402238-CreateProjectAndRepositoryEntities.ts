import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateProjectAndRepositoryEntities1582294402238 implements MigrationInterface {
    name = 'CreateProjectAndRepositoryEntities1582294402238'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `repository` (`id` varchar(36) NOT NULL, `displayName` varchar(255) NOT NULL, `name` varchar(255) NOT NULL, `projectId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `project` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `description` text NOT NULL, `owner` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `repository` ADD CONSTRAINT `FK_ca4afd1a0cf307ea8ba7b76ef06` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `repository` DROP FOREIGN KEY `FK_ca4afd1a0cf307ea8ba7b76ef06`", undefined);
        await queryRunner.query("DROP TABLE `project`", undefined);
        await queryRunner.query("DROP TABLE `repository`", undefined);
    }

}
