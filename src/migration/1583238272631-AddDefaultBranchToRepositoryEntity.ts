import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDefaultBranchToRepositoryEntity1583238272631 implements MigrationInterface {
    name = 'AddDefaultBranchToRepositoryEntity1583238272631'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `repository` ADD `defaultBranch` varchar(255) NOT NULL DEFAULT 'master'", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `repository` DROP COLUMN `defaultBranch`", undefined);
    }

}
