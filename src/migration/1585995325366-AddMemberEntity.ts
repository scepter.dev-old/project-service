import {MigrationInterface, QueryRunner} from "typeorm";

export class AddMemberEntity1585995325366 implements MigrationInterface {
    name = 'AddMemberEntity1585995325366'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `member` (`id` varchar(36) NOT NULL, `user` varchar(255) NOT NULL, `projectId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `member` ADD CONSTRAINT `FK_1521f298c02c827852ebb2aef72` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `member` DROP FOREIGN KEY `FK_1521f298c02c827852ebb2aef72`", undefined);
        await queryRunner.query("DROP TABLE `member`", undefined);
    }

}
