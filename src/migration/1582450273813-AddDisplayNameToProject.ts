import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDisplayNameToProject1582450273813 implements MigrationInterface {
    name = 'AddDisplayNameToProject1582450273813'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` ADD `displayName` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `credential` ADD UNIQUE INDEX `IDX_7c22d82452477b24f10fa05a34` (`username`)", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `credential` DROP INDEX `IDX_7c22d82452477b24f10fa05a34`", undefined);
        await queryRunner.query("ALTER TABLE `project` DROP COLUMN `displayName`", undefined);
    }

}
