import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateUniqueAttribute1582469612500 implements MigrationInterface {
    name = 'UpdateUniqueAttribute1582469612500'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP INDEX `IDX_7c22d82452477b24f10fa05a34` ON `credential`", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_7c22d82452477b24f10fa05a34` ON `credential` (`username`)", undefined);
    }

}
