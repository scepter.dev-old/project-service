import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCredentialEntity1582407382689 implements MigrationInterface {
    name = 'AddCredentialEntity1582407382689'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `credential` (`id` varchar(36) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `repositoryId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `credential` ADD CONSTRAINT `FK_988511550194e1f324cd100cfb0` FOREIGN KEY (`repositoryId`) REFERENCES `repository`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `credential` DROP FOREIGN KEY `FK_988511550194e1f324cd100cfb0`", undefined);
        await queryRunner.query("DROP TABLE `credential`", undefined);
    }

}
