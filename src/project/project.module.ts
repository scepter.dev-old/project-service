import { HttpModule, Module } from "@nestjs/common";
import { ProjectService } from './project.service';
import { ProjectController } from './project.controller';
import {RepositoryService} from "../repository/repository.service";

@Module({
  imports: [HttpModule],
  providers: [ProjectService, RepositoryService],
  controllers: [ProjectController]
})
export class ProjectModule {}
