import {Body, Controller, Delete, Get, Param, Post, Put, Req, Res, UnauthorizedException} from '@nestjs/common';
import {Project} from "../entity/Project";
import {ProjectService} from "./project.service";
import {RepositoryService} from "../repository/repository.service";
import {Repository} from "../entity/Repository";
import {Credential} from "../entity/Credential";

@Controller('projects')
export class ProjectController {
    constructor(private readonly projectService: ProjectService, private readonly repositoryService: RepositoryService) {}

    @Get()
    list(@Req() req, @Res() res): Promise<Project[]>{
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.projectService.list(req.headers.user, req.query.ids).then(projects => {
            return res.status(200).json(projects);
        });
    }

    @Get(':id')
    get(@Req() req, @Res() res, @Param('id') id): Promise<Project>{
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.projectService.get(id, req.headers.user).then(project => {
            return res.status(200).json(project);
        });
    }

    @Post()
    create(@Req() req, @Res() res, @Body() project: Project): Promise<Project>{
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.projectService.create(project.displayName, project.name, project.description, req.headers.user).then(project => {
            return res.status(201).json(project);
        });
    }

    @Put()
    update(@Req() req, @Res() res, @Param('id') id: string, @Body() project: Project): Promise<Project> {
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.projectService.update(project.id, project.displayName, project.name, project.description, req.headers.user, project.members).then(project => {
            return res.status(200).json(project);
        });
    }

    @Put(':id/members')
    updateMembers(@Req() req, @Res() res, @Param('id') id: string,) {

    }

    @Delete(':id')
    remove(@Req() req, @Res() res, @Param('id') id: string): Promise<Project> {
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.projectService.remove(id, req.headers.user).then(() => {
            return res.status(204).json();
        });
    }

    @Post('/:project/repositories')
    createRepository(@Req() req, @Res() res, @Param('project') project: string, @Body() repository: Repository): Promise<Project>{
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.repositoryService.create(repository.name, repository.displayName, project, req.headers.user).then(repository => {
            return res.status(201).json(repository);
        });
    }

    @Delete(':projectId/repositories/:repoId')
    async deleteRepository(@Req() req, @Res() res, @Param('projectId') projectId: string, @Param('repoId') repoId: string) {
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.repositoryService.remove(projectId, repoId, req.headers.user).then(repository => {
            return res.status(204).json(repository);
        });
    }

    @Get(':projectId/repositories/:repoId/credentials')
    async listCredentials(@Req() req, @Res() res, @Param('projectId') projectId: string, @Param('repoId') repoId: string): Promise<Credential[]> {
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.repositoryService.listCredentials(projectId, repoId, req.headers.user).then(repositories => {
            repositories.forEach(repo => {
               delete repo.password;
            });
            return res.status(200).json(repositories);
        });
    }

    @Post(':projectId/repositories/:repoId/credentials')
    async createCredentials(@Req() req, @Res() res, @Param('projectId') projectId: string, @Param('repoId') repoId: string, @Body() credential: Credential): Promise<Credential>{
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.repositoryService.createCredential(projectId, repoId, credential.username, credential.password, req.headers.user).then(credential => {
            delete credential.password;
            return res.status(200).json(credential);
        });
    }

    @Delete(':projectId/repositories/:repoId/credentials/:credentialId')
    async deleteCredentials(@Req() req, @Res() res, @Param('projectId') projectId: string, @Param('repoId') repoId: string, @Param('credentialId') credentialId: string) {
        if(!req.headers.user) {
            throw new UnauthorizedException();
        }
        return this.repositoryService.deleteCredential(projectId, repoId, credentialId, req.headers.user).then(() => {
            return res.status(204).json();
        });
    }

    @Post(':projectName/repositories/:repositoryName/credentials/verify')
    async verifyCredentials(@Req() req, @Res() res, @Param('projectName') projectName: string, @Param('repositoryName') repositoryName: string, @Body() credential: Credential): Promise<Boolean>{
        await this.repositoryService.verifyCredentials(projectName, repositoryName, credential.username, credential.password);
        return res.status(200).json();
    }

}
