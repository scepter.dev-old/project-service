import {
    BadRequestException,
    ConflictException,
    ForbiddenException,
    HttpService,
    Injectable,
    NotFoundException
} from "@nestjs/common";
import { Project } from "../entity/Project";
import { In } from "typeorm";
import { Member } from "../entity/Member";

@Injectable()
export class ProjectService {

    async list(user: string, ids: string[]): Promise<Project[]>{

        if(ids) {
            return Project.find({where: {owner: user, id: In(ids)}})
        }

        return await Project.find({ where: { owner: user }, relations: ["repositories", "members"] });
    }

    async get(projectId: string, user: string): Promise<Project> {
        const project = await Project.findOne({where: {id: projectId, owner: user}, relations: ["repositories", "repositories.credentials", "members"]});

        if(!project) {
            throw new NotFoundException();
        }

        return project;
    }

    async create(displayName: string, name: string, description: string, owner: string): Promise<Project> {
        if(displayName === "" || name === "") {
            throw new BadRequestException("empty_field");
        }

        const foundProject = await Project.findOne({where: {name}});
        if(foundProject) {
            throw new ConflictException("project_already_exists");
        }

        const member = new Member();
        member.user = owner;

        const project = new Project();
        project.displayName = displayName;
        project.name = name;
        project.description = description;
        project.owner = owner;
        project.members = [member];

        await project.save();
        return project;
    }

    async update(id: string, displayName: string, name: string, description: string, user: string, members: Member[]): Promise<Project> {
        if(displayName === "" || name === "") {
            throw new BadRequestException("empty_field");
        }

        const project = await Project.findOne({where: {id}});

        if(!project) {
            throw new NotFoundException();
        }

        if(user !== project.owner) {
            throw new ForbiddenException();
        }

        project.displayName = displayName;
        project.name = name;
        project.description = description;
        project.members = members;
        await project.save();
        return project;
    }

    async remove(id: string, user: string): Promise<Project> {
        const project = await Project.findOne({where: {id}});

        if (!project) {
            throw new NotFoundException();
        }

        if(user !== project.owner) {
            throw new ForbiddenException();
        }

        await project.remove();
        return project;
    }
}
