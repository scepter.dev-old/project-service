import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import { ProjectModule } from './project/project.module';
import { RepositoryService } from './repository/repository.service';
import { RepositoryModule } from './repository/repository.module';
import { DevModule } from './dev/dev.module';
import * as config from "./ormConfig";

@Module({
  imports: [
    TypeOrmModule.forRoot(config),
    ProjectModule,
    RepositoryModule,
    DevModule,
  ],
  controllers: [AppController],
  providers: [AppService, RepositoryService],
})
export class AppModule {}
