import {Controller, Get, Req} from '@nestjs/common';
import {Project} from "../entity/Project";
import {Repository} from "../entity/Repository";
import {Credential} from "../entity/Credential";

@Controller('dev')
export class DevController {

    @Get("clear")
    async clearDb() {
        await Credential.delete({});
        await Repository.delete({});
        await Project.delete({});
    }

    @Get("project")
    async makeProject() {
        const timestamp = Date.now();
        const project = new Project();
        project.displayName = `project${timestamp}`;
        project.name = `project${timestamp}`;
        project.description = `project${timestamp}`;
        project.owner = 'vS2oGnC1I5SEnOtoHlHSXvNHZli1';
        return project.save();
    }
}
